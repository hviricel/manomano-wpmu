��    7      �  I   �      �     �     �     �     �     �     �     �     �               $     0     >  &   D     k  C   y     �     �     �     �     �  b   �     V     [     a  $   s  *   �     �     �     �     �     �                    '     -  !   3     U     ^     r  A   y  F   �  9     A   <     ~  +   �  	   �     �  	   �     �     �     �     �  E  �     4
     <
  
   D
     O
     ^
     d
     �
     �
     �
     �
     �
     �
     �
  9   �
     '  J   8     �     �     �     �     �  o   �     4     ;     A  "   Z  (   }     �     �     �     �     �     �               )     6  '   <  
   d     o     �  X   �  m   �  P   \  Y   �       @        P     Y     _     m     v     z     �                            6   &      0      "   )                 5   $   2      
          #                 -      .   (   3                                         !   ,                            '   4      +       %   	   1       /                *      7    Advanced Basic Category Checkbox Choice Classic Editor (WYSIWYG) Code Editor Color Picker Content Date Time Picker Description Documentation Email Error uploading file. Please try again Example Block Example block that helps you to get started with Lazy Blocks plugin Export Export / Import Export “%1$s” File Gallery Gutenberg blocks visual constructor. Custom meta fields or blocks with output without hard coding. Icon Image Import file empty Imported %s block Imported %s blocks Imported %s template Imported %s templates Incorrect file type Inner Blocks Invalid block. Layout Lazy Blocks No file selected Number Password Radio Range Render callback is not specified. Repeater Rich Text (WYSIWYG) Select Sorry, you are not allowed to read Gutenberg blocks as this user. Sorry, you are not allowed to read Gutenberg blocks data as this user. Sorry, you are not allowed to read Gutenberg blocks data. Sorry, you are not allowed to read Gutenberg blocks of this post. Template Template for post type '%s' already exists. Templates Text Text Area Toggle URL https://nkdev.info/ nK PO-Revision-Date: 2020-07-02 12:48:35+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fr
Project-Id-Version: Plugins - Custom Blocks Constructor &#8211; Lazy Blocks - Stable (latest release)
 Avancé Basique Catégorie Case à cocher Choix Éditeur classique (WYSIWYG) Éditeur de code Sélecteur de couleur Contenu Sélecteur de date et heure Description Documentation E-mail Erreur de téléversement de fichier. Veuillez réessayer Bloc d’exemple Bloc d’exemple qui vous aide à démarrer avec l’extension Lazy Blocks Exporter Exporter/importer Exporter « %1$s » Fichier Galerie Constructeur visuel de blocs Gutenberg. Champs de méta personnalisés ou blocs avec sortie sans codage en dur. Icône Image Importer un fichier vide Importé %s bloc Importé %s blocs Importé %s modèle Importé %s modèles Type de fichier incorrect Blocs internes Bloc non valide. Mise en page Lazy Blocks Aucun fichier sélectionné Nombre Mot de passe Bouton radio Plage Render callback n’est pas spécifié. Répéteur Texte enrichi (WYSIWYG) Sélectionner Désolé, vous n’êtes pas autorisé à lire les blocs Gutenberg avec cet utilisateur. Désolé, vous n’êtes pas autorisés à lire les données des blocs Gutenberg en tant que cet utilisateur. Désolé, vous n’êtes pas autorisé à lire les données des blocs Gutenberg. Désolé, vous n’êtes pas autorisés à lire les blocs Gutenberg de cette publication. Modèle Le modèle pour le type de publication « %s » existe déjà. Modèles Texte Zone de texte Permuter URL https://nkdev.info/ nK 