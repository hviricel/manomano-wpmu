<header id="section-18-12" class=" ct-section header">
  <div class="ct-section-inner-wrap">
    <a id="link-7-9" class="ct-link" href="{{ home_url('/') }}">
      <img id="image-6-9" alt="" src="/wp-content/uploads/sites/2/2021/03/logo_manomano_blanc.png" class="ct-image">
    </a>
    <nav id="_nav_menu-8-9" class="oxy-nav-menu  oxy-nav-menu-dropdowns oxy-nav-menu-dropdown-arrow">
      <div class="oxy-menu-toggle">
        <div class="oxy-nav-menu-hamburger-wrap">
          <div class="oxy-nav-menu-hamburger">
            <div class="oxy-nav-menu-hamburger-line"></div><div class="oxy-nav-menu-hamburger-line"></div><div class="oxy-nav-menu-hamburger-line"></div>
          </div>
        </div>
      </div>
      <div class="menu-header-container">
        <ul id="menu-header" class="oxy-nav-menu-list">
          @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
          @endif
        </ul>
      </div>
    </nav>
  </div>
</header>
